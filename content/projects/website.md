---
title: This Website
description: A basic portfolio website. You're looking at it.
thumbnailPath: WebsitePreview.png
stats: {
	type: Website,
	status: Ongoing,
	software: [Vim, Vue, Nuxt],
	languages: [HTML, CSS, JS],
	links: [{label: Source, href: https://gitlab.com/shahanstudios/portfolio-website}]
}
type: WebDev
order: -100
---
