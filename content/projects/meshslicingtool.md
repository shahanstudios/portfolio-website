---
title: Mesh Slicing Tool
description: A Unity3D Tool to slice 3D meshes into stretchable and tileable submeshes akin to a 9-slice image.
thumbnailPath: MeshSlicingToolPreview.png
stats: {
	type: Tool,
	status: Ongoing,
	duration: 39 hours,
	software: [Unity3D],
	languages: [C#],
	links: [{label: Source, href: https://gitlab.com/shahanstudios/meshslicingtool}]
}
type: GameDev
order: 0
---

# Summary

This project was the creation of a mesh slicing tool for Unity3D that allowed a user to slice any given mesh and submeshes into up to 27 different pieces that scale independently based on the parent scale and different scaling parameters.

The submesh pieces keep the materials, normals and UVs at the cut.

<req-img src="assets/images/MeshSlicingToolPreview.png"></req-img>
<req-img src="assets/images/BasePlatformStartingPoint.png"></req-img>


<details><summary><h1>Background</h1></summary>

In an earlier prototype for a [Breakout](<https://en.wikipedia.org/wiki/Breakout_(video_game)>) inspired game, I had wondered about how to deal with upgrades that grow or shrink the paddle length.

From a size and collision standpoint, it seemed simple to just increase the scale of the paddle object to fit a desired length. I knew that if I were to care at all about how the paddle would be rendered, I would have to use a different solution as uniformly scaling the mesh would result in paddle graphics that stretched or shrunk in a manner that would be unappealing.

The solution I had envisioned was to attempt the 2D image scaling technique known as [9-slice scaling](https://en.wikipedia.org/wiki/9-slice_scaling) and add a 3rd dimension to it.
![Wikipedia - Traditional scaling vs 9-slice scaling](https://upload.wikimedia.org/wikipedia/commons/7/7a/Traditional_scaling_vs_9-slice_scaling.svg)

</details>

<details open><summary><h1>Details</h1></summary>

<details open>
	<summary><h2>Step 1 - Mesh Slicing</h2></summary>

While this was not the first project I have done creating and modifying meshes dynamically, this was the first project I've done that attempts to cut a mesh and its data into multiple visually similar meshes.

The idea of using a plane to cut a mesh in two was not a difficult problem in concept, but I did use some resources to help me understand how to split an edge and the best way to create new triangles to fill the gap.  
Those resources are as follows:  
https://www.youtube.com/watch?v=1UsuZsaUUng https://github.com/hugoscurti/mesh-cutter

The rest of the difficulty for this step lay in figuring out how best to iterate through the cuts to correctly slice through the mesh to produce all the desired cut pieces.

To solve this, I grouped the cuts by their orientation plane and sliced the mesh with all the cuts of a given orientation before moving on to the next one. For a given orientation, a cut would work on the intermediate pieces left over from the previous cut.
Once all the cuts had been applied, then the remaining pieces were set up to either scale or tile appropriately.

</details>

<details>
	<summary><h2>Step 2 - Slice Scaling</h2></summary>

<req-img src="assets/images/ScaledUVExample.png"></req-img>

After getting basic mesh slicing in order, the next step was to define how the sub-mesh pieces were to scale and position themselves based off of their parent.  
Like a 9-slice image, the corners were always going to be positioned at the bounds extents, and always stay the same size. This led to the scale of the corner pieces simply being the inverse scale of the parent.

The difficulty lay in how to answer two questions:

1. What is the best way to determine the primary axes for edges and faces?
2. What is the best way to scale edges and faces along their primary axes?

**A1.** To determine the primary axes for a given piece, I made an assumption about the relative centers of the piece and the parent mesh. If the distance between the piece center and the parent center was 0 for a given axis, then the piece should scale to fit the remaining space for the given axis. If it was not zero, it should scale that axis to the inverse of its parent.  
This had the benefit of simplifying the scaling of the corners as their distance from the center was nonzero for all axes.

```cs
private Vector3 GetDirectionToParentCenter() {
	float epsilon = 0.001f;
	Vector3 diff = parentBounds.center - bounds.center;
	return new Vector3(Math.Sign(diff.x), Math.Sign(diff.y), Math.Sign(diff.z));
}
```

```cs
protected override void UpdatePieceScale(Vector3 scale) {
	Bounds scaledParentBounds = new Bounds(parentBounds.center, Vector3.Scale(parentBounds.size, scale));
	Vector3 invScale = scale.InvertComponents();
	Vector3 newScale = GetNewPieceScale(scale);
	transform.localScale = new Vector3(
			dirToParentCenter.x != 0 ? invScale.x : newScale.x,
			dirToParentCenter.y != 0 ? invScale.y : newScale.y,
			dirToParentCenter.z != 0 ? invScale.z : newScale.z);

	Vector3 parentScaleFactor = -Vector3.Scale(dirToParentCenter, parentBounds.extents);
	Vector3 localScaledSize = Vector3.Scale(dirToParentCenter, Vector3.Scale(bounds.size, transform.localScale));
	transform.localPosition = parentScaleFactor + localScaledSize;
}
```

**A2.** Determining the new scale was the simpler question to answer as it was a question about calculating the space needing filled. To calculate the remaining space, an assuption was made that the space leftover from the initial size was due to the corners, and thus that size would not change based on scale. The new space to fill was the scaled parent size - the initial leftovers. So the new scale was simply the multiplier to make the initial size of the piece equal to the space to fill.

```cs
protected Vector3 GetSpaceToFill(Vector3 scale) {
	Vector3 scaledParentSize = Vector3.Scale(parentBounds.size, scale);

	if (bounds.size.Equals(Vector3.zero)) {
		return scaledParentSize;
	}

	Vector3 baseScaleLeftovers = parentBounds.size - bounds.size;
	return scaledParentSize - baseScaleLeftovers;
}
```

```cs
private Vector3 GetNewPieceScale(Vector3 scale) {
	Vector3 spaceToFill = GetSpaceToFill(scale);
	if (bounds.size == Vector3.zero) {
		bounds.size = Vector3.one;
	}
	Vector3 newScale = spaceToFill.InverseScale(Vector3.Scale(bounds.size, scale));

	return newScale;
}
```

</details>

<details><summary><h2>Step 3 - Slice Tiling</h2></summary>

<req-img src="assets/images/TiledUVExample.png"></req-img>

Once mesh pieces were scaling correctly, the next step was to create an option to allow the pieces to tile to fit instead of scaling. The reasoning behind this was to make sure that geometry and uvs remained the same size as the base geometry to hopefully create a better appearance.  
This was a similar process to stretching the tiles, so code was able to be refactored and extended to achieve the result. Instead of stretching though, each tiling piece kept a 2D list of children objects to tile in a grid along the primary stretch axes of the piece.  
If the amount of tiled children was not a whole number, the last piece was cut using the same process as the entire mesh, only keeping the portion inside the mesh.

</details>

<details>
	<summary><h2>Step 4 - Custom Editor Window</h2></summary>

After the primary functionality was established, the last step was to create a custom editor window to enable updating the cuts on the base mesh in an easier fashion.

Unity's custom editor functionality does not have a strong documentation foundation compared to their other docs. This made figuring out how to achieve what I wanted the editor to look like more difficult than desired.  
The documentation has enough to get started creating windows and GUI components, but what I really wanted was to display the base mesh in context with editor handles that can be dragged to adjust the cut position. This was more difficult to find, and after watching a handful of Youtube videos and scouring the forums, I finally found the [exact discussion](https://forum.unity.com/threads/writing-an-editor-window-that-mimics-the-scene-view.613231/) I was looking for.  

This allowed me to create the editor window scene below.
<req-img src="assets/images/EditorWindow.png"></req-img>

After that was accomplished, I wanted to update the inspector view to achieve edit parity between the custom window and the custom inspector.  
<req-img src="assets/images/InspectorView.png"></req-img>

</details>

</details>

<details>
	<summary><h1>What's Next?</h1></summary>
	
I hope to package the tool up nicely and publish it to the Unity Asset Store for a first release.

</details>
