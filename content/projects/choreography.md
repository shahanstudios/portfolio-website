---
title: Choreography
description: A platformer game prototype about indirectly controlling a robot by modifying its signal curves over time.
thumbnailPath: Choreography.png
stats: {
	type: Game,
	status: Prototype,
	duration: 67 hours,
	software: [Unity3D, Blender],
	languages: [C#],
	links: [{label: Source, href: https://gitlab.com/shahanstudios/choreography},
	{label: Itch.io, href: https://shahanstudios.itch.io/choreography}]
}
type: GameDev
order: -2
---

# Summary  

**Choreography** is a game prototype I had thought up based around the idea of moving a character
not through direct input, but through choreographing (hence the name) the player's movement along different timeline
tracks.

<iframe src="https://itch.io/embed/675076?dark=true" width="552" height="167" frameborder="0"><a href="https://shahanstudios.itch.io/choreography">Choreography by Shahan Studios</a></iframe>

<req-img src="assets/images/ChoregraphyTimeline.png"></req-img><br />

# Details

The main system of Choreography is the Track system.  
in Choreography there exists a timeline for each room that governs the player's movement for that room. The timeline contains Tracks that are tied to specific aspects of player movement. For example, in the current build of Choreography, there exists a **Movement** track tied to the player's horizontal velocity and a **Jumping** track that designates when and with how much power the player is to jump.

<req-img src="assets/images/ChoreographyMovement.gif"></req-img><br />

Choreography's gameplay loop has two main phases: Planning and Execution.  
During the planning phase, the player uses a controller or keyboard / mouse to create, move, or delete points as long as the track is not locked.  
The Execution phase is when the level activates; the player moves and responds to the tracks, and the environment also updates based on the timeline.

<req-img src="assets/images/ChoreographyJumping.gif"></req-img><br />

I had also thought up a few other track ideas, such as a **Boost** track to add a temporary burst of speed to the player for a specified duration. Another idea was a **Thruster** track to allow sustained vertical ascend and descent. These track ideas could have different wave patterns such as the sawtooth or square waves.

The initial concept was as a sort of puzzle-platformer metroidvania game, so these tracks were considered to be upgrades further along. These additional tracks could provide the opportunity for alternate routes in rooms.
Within the early narrative there was also a concept of tracks being locked as the game progresses and the player-character gains agency. For a given room or scenario, a certain track (say the **Jumping** track) or track portion would have a pre-set waveform already set and it would be locked from user modification. This would force a limitation onto the player to work around which I think could be interesting design space.

There were other unanswered design considerations, such as how to alleviate trial and error or mitigate tedium with backtracking and having to redo tracks the player has already completed.

# What's Next?

I still very much like the idea presented here as well as found it an enjoyable and challenging system to program. It is one that I very well could return to at some point, but I think that it would serve better as a straight puzzle-platformer rather than a full metroidvania to minimize frustration. That would allow more focus on the core puzzle design of how to use various tracks and deal with locked tracks to solve a room.
