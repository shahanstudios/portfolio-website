---
title: Control Chamber
description: My entry to the 2020 GMTK Game Jam.
thumbnailPath: ControlChamber05.png
stats: {
	type: Game,
	status: Prototype,
	duration: 17 hours,
	software: [Unity3D, Aseprite],
	languages: [C#],
	links: [{label: Source, href: https://gitlab.com/shahanstudios/gmtk_jam_2020},
	{label: Itch.io, href: https://shahanstudios.itch.io/control-chamber}]
}
type: GameDev
order: -1
---

# Summary

**Control Chamber** is my solo entry to the 48h 2020 GMTK Game Jam. The theme for this year was **"Out of
Control"**.  

<iframe src="https://itch.io/embed/695576?dark=true" width="552" height="167" frameborder="0"><a href="https://shahanstudios.itch.io/control-chamber">Control Chamber by Shahan Studios</a></iframe><br />

# Details

After an hour of brainstorming, I came up with the idea discussed below. Control Chamber is a simple top-down puzzle room game, where the objective is to move your character to the level goal without entering EMP zones designated in yellow.

Since this was my first ever game jam, I had decided to attempt to keep the scope restrained by going with a top-down, tile-based level map with simple grid-based movement. The level pieces also only processed themselves on a tick system that triggered when the player moved or waited.

<req-img src="assets/images/ControlChamber01.png"></req-img>

If you enter the EMP zone, your character will be unable to move until you exit the EMP zone, so if there is no external way to leave the zone, you have to restart the level.

After I had made the basic level concept, next came adding different block designs in the level that interact with the player and the EMP projectors.
This is where scope creep slowly started to grow.

First, I added conveyor belts, that move whatever entity is above them when the game ticks. I figured this could move the player through EMP zones passively, could move blocks around the level, or move EMPs away from the player to enable control again.

<req-img src="assets/images/ControlChamber02.png"></req-img>

This was fairly simple to achieve, but I didn't find the design space to be very deep with only this addition. It is also important to note that this was my first in-depth attempt to create interesting levels, so my experience in that department is fairly limited, but I am happy with what I was able to create in the time allotted.

The next system to be created was probably where most of my time got sunk. I intended (and in part succeeded) in creating a logic and wiring system. This would allow for electrical entities such as the conveyor belt and EMP projectors to have on and off states that could be activated with specific buttons and triggers.

<req-img src="assets/images/ControlChamber03.png"></req-img>

The logic and wiring is really where the scope creep set in.  

Due to an early decision to make the game with top-down geometry instead of sprites, every object in the game was created with a quad mesh. This made rendering the wires more frustrating, since I ended up spending a lot of time creating a custom shader to render the correct orientation using bit-flags to determine direction.  
I would correctly say that I am a novice when it comes to shaders (although it is something I think I am very interested in), so creating the shader took a lot longer than I initially hoped, coupled with the fact that I haven't used bit-flags in awhile, so it took me a handful of tries to get the connection logic correct.

I also realized late in the jam that my logic system was pretty rudimentary and kept me from adding additional logic features to assist with level design. I spent far too long trying to hack together a generic logic inversion, so that wires and EMPs could create an inverted signal when activated. The inversion principle was not hard to achieve, but it was not intuitively reactive to the incoming logic signal, so I decided to scrap it.

After deciding to let the logic system be, I decided to add another staple of the Portal-esque test-chamber genre, being movable blocks and block puzzles, so that was the last simple addition to the game.

<req-img src="assets/images/ControlChamber05.png"></req-img>

# What's Next?

While I agree with some of the comments I had received from the jam that this would make a good mobile game idea, I don't think I am going to pursue that any time soon.
I could revisit it just as an educational exercise to better design the logic system and some ideas left out from the jam (such as remote wire triggering with lasers), but that learning will probably be achieved through other projects instead.
